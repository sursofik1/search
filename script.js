const perPage = 10;
const page = 0;
const sort = 'stars';
const state = {
    repositories: [],
    isAlertEnabled: false,
    notFound: false
}
function formatDate(data){
    const options = {
        year: "numeric",
        month: "long",
        day: "numeric"
    };
    return data.toLocaleString(options);
}
function renderValidationAlert(){
    if (state.isAlertEnabled){
        return
    }
    let el = document.getElementById('new-repository');
    let el2 = document.createElement('div');
    el2.classList.add('alert');
    el2.id = 'alert';
    el2.textContent = '️Введите 2 или более символов';
    el.appendChild(el2);
    state.isAlertEnabled = true
}
function hideValidationAlert(){
    if (state.isAlertEnabled){
        let container = document.getElementById('new-repository');
        let el = document.getElementById('alert');
        container.removeChild(el);
        state.isAlertEnabled = false
    }
}
function notFound(){
    let el = document.getElementById('new-repository');
    let el2 = document.createElement('div');
    el2.classList.add('alert');
    el2.id = 'alert';
    el2.textContent = 'Ничего не найдено';
    el.appendChild(el2);
    state.notFound = true
}
function hideNotFound() {
    if (state.notFound) {
        let container = document.getElementById('new-repository');
        let el = document.getElementById('alert');
        container.removeChild(el);
        state.notFound = false
    }
}
function showError(err){
    alert("Произошла ошибка сервера, повторите попытку позже")
    console.error(err)
}
function onButtonClick(){
    const query = document.getElementById('search').value
    console.log(query.trim().length < 2)
    if (query.trim() === '' || query.trim().length < 2){
        renderValidationAlert()
        return;
    }
    getRepositories(query)
}
function renderRepositories(repository){
    let newRepository = document.createElement('div')
    newRepository.classList.add('new-repository-box');
    let newRepositoryBox1 = document.createElement('div');
    newRepositoryBox1.classList.add('new-repository-box1');
    let newRepositoryName = document.createElement('a');
    newRepositoryName.classList.add('new-repository-name');
    newRepositoryName.textContent = repository.name;
    newRepositoryName.href = repository.url;
    newRepositoryName.setAttribute('target', '_blank')
    newRepositoryBox1.appendChild(newRepositoryName);
    let newRepositoryDescription = document.createElement('p');
    newRepositoryDescription.classList.add('new-repository-description');
    newRepositoryDescription.textContent = repository.description
    newRepositoryBox1.appendChild(newRepositoryDescription)
    let newRepositoryBox2 = document.createElement('div')
    newRepositoryBox2.classList.add('new-repository-box2');
    let newRepositoryDate = document.createElement('p');
    newRepositoryDate.classList.add('new-repository-date');
    newRepositoryDate.textContent = formatDate(repository.date);
    newRepositoryBox2.appendChild(newRepositoryDate);
    let star = document.createElement('img');
    star.src = './asset/pngimg.com - star_PNG41451.png';
    star.classList.add('star')
    newRepositoryBox2.appendChild(star)
    let newRepositoryStars = document.createElement('p');
    newRepositoryStars.classList.add('new-repository-stars');
    newRepositoryStars.textContent = repository.stars;
    newRepositoryBox2.appendChild(newRepositoryStars);
    newRepositoryBox1.appendChild(newRepositoryBox2)
    newRepository.appendChild(newRepositoryBox1);
    document.getElementById('new-repository').appendChild(newRepository);

}
function getRepositories(query) {
    fetch( `https://api.github.com/search/repositories?q=${query}&per_page=${perPage}&page=${page}&sort=${sort}`)
        .then(res => res.json())
        .then(data => {
            state.repositories = []
            for (let item of data.items){
                state.repositories.push(map(item))
            }
            console.log(state.repositories)
            render()
        })
        .catch(err => showError(err))
}
function map(item){
    return {
        name: item.full_name,
        description: item.description,
        url: item.svn_url,
        date: new Date(item.created_at),
        stars: item.stargazers_count
    }
}
function render() {
    hideNotFound()
    let container = document.getElementById('new-repository');
    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }
    if (state.repositories.length === 0) {
        notFound();
        return;
    }
    for (let repository of state.repositories) {
        renderRepositories(repository);
    }
}
let el = document.getElementById('button');
el.addEventListener('click', onButtonClick);

function onClickEnter(event){
    if (event.code === 'Enter')
    {
        onButtonClick()
    }
}
document.getElementById('search')
    .addEventListener('keyup', onClickEnter)

let elSearch = document.getElementById('search')
elSearch.addEventListener('input', hideValidationAlert)

